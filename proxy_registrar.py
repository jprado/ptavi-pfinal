#!/usr/bin/python3
# Jorge Prado Fernandez
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib
import json
import random
import socket
import socketserver
import sys
import time

if len(sys.argv) != 2:
    sys.exit("Usage: python3 proxy_registrar.py config")


def fichLog(mensaje):
    """
    Funcion para escribir en ficheros log
    """
    fichero = open(FICHERO_LOG, 'a')
    tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    mensaje2 = mensaje.replace("\r\n", " ")
    fichero.write(tiempo + " " + mensaje2 + "\r\n")
    fichero.close()


class UniversalHandler(ContentHandler):
    """
    Clase manejadora
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.dic2 = {}

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'server':
            self.dic2['PORT'] = attrs.get('puerto', "")
            self.dic2['IP'] = attrs.get('ip', "")
        if name == 'database':
            self.dic2['PASSWDPATH'] = attrs.get('passwdpath', "")
        if name == 'database':
            self.dic2['PATH'] = attrs.get('path', "")
        if name == 'log':
            self.dic2['FICHERO_LOG'] = attrs.get('path', "")

    def get_tags(self):
        return self.dic2


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dic = {}
    passdic = {}
    numero = str(random.randint(1000000, 1000000000))

    def json2registered(self):
        """
        Buscamos el archivo json en nuestra carpeta, si existe, usamos los
        datos contenidos como nuestro diccionario actual, si no existe pasamos
        """
        try:
            with open("registered.json") as fich:
                self.dic = json.load(fich)
        except FileNotFoundError:
            pass

    def json2passwords(self):
        """
        Vamos a leer el fichero de las contraseñas para luego extraerlas
        """
        try:
            with open(PASSWDPATH) as fich2:
                self.passdic = json.load(fich2)
        except FileNotFoundError:
            pass

    def deluser(self):
        """
        Bucle para eliminar usuarios del diccionario
        """
        lista = []
        actual_time = (time.time())

        for user in self.dic:
            if int(actual_time) >= int(self.dic[user]['expires']):
                lista.append(user)
        print("Usuarios que se eliminan")
        print(lista)
        print("")
        for user in lista:
            del self.dic[user]

    def register2json(self):
        """
        Bloque para crear archivos json
        """
        with open("registered.json", "w") as fich:
            json.dump(self.dic, fich, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registered()
        line = self.rfile.read()
        print('')
        petition = line.decode('utf-8')
        print("El cliente nos manda ", line.decode('utf-8'))
        mensaje = "Received from " + IP + ":puerto: "
        mensaje += petition
        fichLog(mensaje)
        phrase = petition.split()

        if phrase[0] == "REGISTER":
            if len(phrase) == 5:
                self.wfile.write(b"SIP/2.0 401 Unauthorized\r\nWWW-Authenticate: Digest nonce='" + bytes(self.numero, 'utf-8') + b"'\r\n\r\n")
                mensaje = "Sent to " + IP + ":puerto: "
                mensaje += "SIP/2.0 401 Unauthorized "
                mensaje += "WWW-Authenticate: Digest nonce='"
                mensaje += bytes(self.numero, 'utf-8')
                fichLog(mensaje)
            else:
                if phrase[0] == "REGISTER" and phrase[5] == "Authorization:":
                    phrase2 = phrase[1].split(':')
                    user = phrase2[-2]
                    port = phrase2[-1]
                    ip = self.client_address[0]
                    expires = int(phrase[4])
                    expired = int(expires) + int(time.time())
                    """
                    Procedimiento para registrar nuevos usuarios
                    y borrar los antiguos
                    """
                    self.json2passwords()
                    password = self.passdic[user]['password']
                    print(password)
                    resumen = hashlib.md5()
                    resumen.update(bytes(self.numero, 'utf-8'))
                    resumen.update(bytes(password, 'utf-8'))
                    HEXNUMBER = resumen.hexdigest()

                    if HEXNUMBER == phrase[8]:
                        self.deluser()

                        if expires == 0:
                            if user in self.dic:
                                del self.dic[user]
                                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                                respuesta = "SIP/2.0 200 OK"
                                mensaje = "Sent to " + IP + ":puerto: "
                                mensaje += respuesta
                                fichLog(mensaje)
                        else:
                            self.dic[user] = {'address': ip, 'port': port, 'expires': expired}

                        self.register2json()
                        """
                        Imprimimos el diccionario y el SIP
                        """
                        print("El diccionario es el siguiente: ")
                        print(self.dic)
                        print("")
                        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                        respuesta = "SIP/2.0 200 OK"
                        mensaje = "Sent to " + IP + ":puerto: "
                        mensaje += respuesta
                        fichLog(mensaje)

                    else:
                        self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                        respuesta = "SIP/2.0 400 Bad Request"
                        mensaje = "Sent to " + IP + ":puerto: "
                        mensaje += respuesta
                        fichLog(mensaje)

        if phrase[0] == "INVITE":
            phrase2 = phrase[1].split(':')
            user = phrase2[-1]
            if user in self.dic:
                SERVER = self.dic[user]['address']
                PORT = self.dic[user]['port']
                LINE = petition

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

                    my_socket.connect((str('localhost'), int(PORT)))
                    print("Enviando:", str(LINE))
                    my_socket.send(bytes(str(LINE), 'utf-8'))
                    mensaje = "Sent to " + IP + ":puerto: "
                    mensaje += LINE
                    fichLog(mensaje)
                    data = my_socket.recv(1024)  # Tamaño del buffer
                    print('Recibido -- ', data.decode('utf-8'))
                    inviteresponse = data.decode('utf-8')
                    self.wfile.write(bytes(inviteresponse, 'utf-8'))
                    mensaje = "Received from " + IP + ":puerto: "
                    mensaje += inviteresponse
                    fichLog(mensaje)
            else:
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n\r\n")
                respuesta = "SIP/2.0 404 User Not Found"
                mensaje = "Sent to " + IP + ":puerto: "
                mensaje += respuesta
                fichLog(mensaje)

        if phrase[0] == "ACK":
            phrase2 = phrase[1].split(':')
            user = phrase2[-1]
            if user in self.dic:
                SERVER = self.dic[user]['address']
                PORT = self.dic[user]['port']
                LINE2 = petition

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

                    my_socket.connect((str('localhost'), int(PORT)))
                    print("Enviando:", str(LINE2))  # LINE2 envia un ack
                    my_socket.send(bytes(str(LINE2), 'utf-8'))
                    mensaje = "Sent to " + IP + ":puerto: "
                    mensaje += LINE2
                    fichLog(mensaje)
                    data = my_socket.recv(1024)  # Tamaño del buffer
                    print('Recibido -- ', data.decode('utf-8'))
                    recibido = data.decode('utf-8')
                    mensaje = "Received from " + IP + ":puerto: "
                    mensaje += recibido
                    fichLog(mensaje)

        if phrase[0] == "BYE":
            phrase2 = phrase[1].split(':')
            user = phrase2[-1]
            if user in self.dic:
                SERVER = self.dic[user]['address']
                PORT = self.dic[user]['port']
                LINE3 = petition

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

                    my_socket.connect((str('localhost'), int(PORT)))
                    print("Enviando:", str(LINE3))  # LINE3 envia un bye
                    my_socket.send(bytes(str(LINE3), 'utf-8'))
                    mensaje = "Sent to " + IP + ":puerto: "
                    mensaje += LINE3
                    fichLog(mensaje)
                    data = my_socket.recv(1024)  # Tamaño del buffer
                    print('Recibido -- ', data.decode('utf-8'))
                    recibido = data.decode('utf-8')
                    mensaje = "Received from " + IP + ":puerto: "
                    mensaje += recibido
                    fichLog(mensaje)
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                mensaje = "Sent to " + IP + ":puerto: "
                mensaje += "SIP/2.0 200 OK"
                fichLog(mensaje)

        if phrase[0] != "REGISTER" and phrase[0] != "INVITE" and phrase[0] != "BYE" and phrase[0] != "ACK":
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            mensaje = "Sent to " + IP + ":puerto: "
            mensaje += "SIP/2.0 400 Bad Request"
            fichLog(mensaje)
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
            mensaje = "Sent to " + IP + ":puerto: "
            mensaje += "SIP/2.0 405 Method Not Allowed"
            fichLog(mensaje)


if __name__ == "__main__":
    # Listens at localhost ('') and in the port that we write in the shell
    # and calls the EchoHandler class to manage the request
    ARCHIVE = sys.argv[1]
    parser = make_parser()
    cHandler = UniversalHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(ARCHIVE))
    config = cHandler.get_tags()

    for elem in config:
        print(elem, config[elem])

    PATH = config['PATH']
    PASSWDPATH = config['PASSWDPATH']
    PORT = int(config['PORT'])
    FICHERO_LOG = config['FICHERO_LOG']
    IP = config['IP']

    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    print("Server MiServidorBigBang listening at port 5555...")
    mensaje = "Starting..."
    fichLog(mensaje)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        mensaje = "Finishing..."
        fichLog(mensaje)
        print("Finalizado servidor")
