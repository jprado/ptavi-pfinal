#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Jorge Prado Fernandez
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

from uaclient import UniversalHandler
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import os
import socketserver
import sys
import time

if len(sys.argv) != 2:
    sys.exit("Usage: python3 uaserver.py config")


def fichLog(mensaje):
    """
    Funcion para escribir en ficheros log
    """
    fichero = open(FICHERO_LOG, 'a')
    tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    mensaje2 = mensaje.replace("\r\n", " ")
    fichero.write(tiempo + " " + mensaje2 + "\r\n")
    fichero.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    dic = {}

    def handle(self):

        # Leyendo línea a línea lo que nos envía el cliente
        line = self.rfile.read()
        print("El cliente nos manda " + line.decode('utf-8'))
        # Metodo para identificar peticiones
        text = line.decode('utf-8')
        mensaje = "Received from " + IP + ":" + PROXY_PORT + ": "
        mensaje += text
        fichLog(mensaje)
        methods = ['INVITE', 'BYE', 'ACK']
        petition = text.split()
        SIP = petition[1].split(':')

        if petition[0] == 'INVITE':
            self.dic['receptor_IP'] = petition[7]
            self.dic['receptor_Puerto'] = petition[11]
            response = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\nSIP/2.0 200 OK\r\n"
            response += "Content-Type: application/sdp\r\n\r\nv=0\r\n"
            response += "o=" + USER + " " + IP + "\r\ns=misesion\r\n"
            response += "t=0\r\nm=audio " + AUDIOPORT + " RTP\r\n"
            self.wfile.write(bytes(response, 'utf-8'))
            mensaje = "Sent to " + IP + ":" + PROXY_PORT + ": "
            mensaje += response
            fichLog(mensaje)

            if SIP[0] != 'sip':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                respuesta = "SIP/2.0 400 Bad Request"
                mensaje = "Error " + IP + ":" + PROXY_PORT + ": "
                mensaje += respuesta
                fichLog(mensaje)
            elif petition[2] != 'SIP/2.0':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                respuesta = "SIP/2.0 400 Bad Request"
                mensaje = "Error " + IP + ":" + PROXY_PORT + ": "
                mensaje += respuesta
                fichLog(mensaje)

        elif petition[0] == 'ACK':
            aEjecutar = './mp32rtp -i ' + self.dic['receptor_IP'] + " -p "
            aEjecutar += self.dic['receptor_Puerto'] + ' < ' + FICHERO_AUDIO
            print("Vamos a ejecutar", aEjecutar)
            os.system(aEjecutar)

        elif petition[0] == 'BYE':
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            respuesta = "SIP/2.0 200 OK"
            mensaje = "Sent to " + IP + ":" + PROXY_PORT + ": "
            mensaje += respuesta
            fichLog(mensaje)

        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")
            respuesta = "SIP/2.0 405 Method Not Allowed"
            mensaje = "Error " + IP + ":" + PROXY_PORT + ": "
            mensaje += respuesta
            fichLog(mensaje)


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    ARCHIVE = sys.argv[1]
    parser = make_parser()
    cHandler = UniversalHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(ARCHIVE))
    config = cHandler.get_tags()
    for elem in config:
        print(elem, config[elem])

    USER = config['USER']
    PORT = int(config['PORT'])
    IP = config['IP']
    AUDIOPORT = config['AUDIOPORT']
    FICHERO_AUDIO = config['FICHERO_AUDIO']
    FICHERO_LOG = config['FICHERO_LOG']
    PROXY_PORT = config['PROXY_PORT']

    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Listening...")
    mensaje = "Starting..."
    fichLog(mensaje)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        mensaje = "Finishing..."
        fichLog(mensaje)
        print("Finalizando servidor")
