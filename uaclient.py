#!/usr/bin/python3
# Jorge Prado Fernandez

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib
import os
import random
import socket
import sys
import time


def fichLog():
    """
    Funcion para escribir en ficheros log
    """
    fichero = open(FICHERO_LOG, 'a')
    tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    mensaje2 = mensaje.replace("\r\n", " ")
    fichero.write(tiempo + " " + mensaje2 + "\r\n")
    fichero.close()


class UniversalHandler(ContentHandler):
    """
    Clase manejadora
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.dic = {}

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'account':
            self.dic['USER'] = attrs.get('username', "")
            self.dic['PASSWD'] = attrs.get('passwd', "")
        elif name == 'uaserver':
            self.dic['IP'] = attrs.get('ip', "")
            self.dic['PORT'] = attrs.get('puerto', "")
        elif name == 'rtpaudio':
            self.dic['AUDIOPORT'] = attrs.get('puerto', "")
        elif name == 'audio':
            self.dic['FICHERO_AUDIO'] = attrs.get('path', "")
        elif name == 'log':
            self.dic['FICHERO_LOG'] = attrs.get('path', "")
        elif name == 'regproxy':
            self.dic['PROXY_PORT'] = attrs.get('puerto', "")

    def get_tags(self):
        return self.dic


if __name__ == "__main__":
    """
    Programa principal
    """

    ARCHIVE = sys.argv[1]
    parser = make_parser()
    cHandler = UniversalHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(ARCHIVE))
    config = cHandler.get_tags()

    for elem in config:
        print(elem, config[elem])

    methods = ['REGISTER', 'INVITE', 'BYE']

    if sys.argv[2] not in methods:
        sys.exit("Usage: python3 uaclient.py config method option")

    # Constantes.

    METHOD = sys.argv[2]
    OPTION = sys.argv[3]
    IP = config['IP']
    USER = config['USER']
    PORT = config['PORT']
    AUDIOPORT = config['AUDIOPORT']
    FICHERO_AUDIO = config['FICHERO_AUDIO']
    FICHERO_LOG = config['FICHERO_LOG']
    PROXY_PORT = config['PROXY_PORT']
    LINE = (METHOD + " sip:" + USER + ":" + PORT + " SIP/2.0" + "\r\n" +
            "Expires: " + OPTION + "\r\n")  # Para register
    LINE2 = (METHOD + " sip:" + OPTION + " SIP/2.0" + "\r\n")  # Para byes
    LINE3 = ("ACK" + " sip:" + OPTION + " SIP/2.0" + "\r\n")  # Para ack

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

    if METHOD == "REGISTER":
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

            my_socket.connect((str('localhost'), 5555))
            print("Enviando:", str(LINE))
            my_socket.send(bytes(str(LINE), 'utf-8') + b'\r\n')
            mensaje = "Sent to " + IP + ":" + PROXY_PORT + ": "
            mensaje += LINE
            fichLog()
            data = my_socket.recv(1024)  # Tamaño del buffer
            print('Recibido -- ', data.decode('utf-8'))
            respuesta = data.decode('utf-8')
            ATHERROR = respuesta.split(" ")[1]
            mensaje = "Received from " + IP + ":" + PROXY_PORT + ": "
            mensaje += respuesta
            fichLog()

            if ATHERROR == "401":
                PASSWD = config['PASSWD']  # Cogemos la contraseña del xml
                NUMERO = respuesta.split("'")[1]  # Numero que envia el proxy
                RESUMEN = hashlib.md5()
                RESUMEN.update(bytes(NUMERO, 'utf-8'))
                RESUMEN.update(bytes(PASSWD, 'utf-8'))
                HEXNUMBER = RESUMEN.hexdigest()
                AUTHORIZATION = (METHOD + " sip:" + USER + ":" + PORT +
                                 " SIP/2.0" + "\r\n" + "Expires: " + OPTION +
                                 "\r\n" + "Authorization: Digest response=' " +
                                 HEXNUMBER + " '\r\n")
                # Procedemos a enviar la autenticación
                my_socket.connect((str('localhost'), 5555))
                print("Enviando:", str(AUTHORIZATION))
                my_socket.send(bytes(str(AUTHORIZATION), 'utf-8') + b'\r\n')
                mensaje = "Sent to " + IP + ":" + PROXY_PORT + ": "
                mensaje += AUTHORIZATION
                fichLog()
                data = my_socket.recv(1024)  # Tamaño del buffer
                print('Recibido -- ', data.decode('utf-8'))
                respuesta = data.decode('utf-8')
                mensaje = "Received from " + IP + ":" + PROXY_PORT + ": "
                mensaje += respuesta
                fichLog()

    if METHOD == "INVITE" or METHOD == "BYE":
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

            if METHOD == 'INVITE':
                LINE4 = (METHOD + " sip:" + OPTION + " SIP/2.0" + "\r\n" +
                         "Content-Type: application/sdp" + "\r\n\r\n" + "v=0" +
                         "\r\n" + "o=" + USER + " " + IP + "\r\n" +
                         "s=misesion" + "\r\n" + "t=0" + "\r\n" + "m=audio " +
                         AUDIOPORT + " RTP")
                my_socket.connect((str('localhost'), 5555))
                print("Enviando:", str(LINE4))
                my_socket.send(bytes(str(LINE4), 'utf-8') + b'\r\n')
                mensaje = "Sent to " + IP + ":" + PROXY_PORT + ": "
                mensaje += LINE4
                fichLog()
                data = my_socket.recv(1024)  # Tamaño del buffer
                respuesta = data.decode('utf-8')
                comprobacion = respuesta.split(" ")
                print(comprobacion)
                print('Recibido -- ', data.decode('utf-8'))
                mensaje = "Received from " + IP + ":" + PROXY_PORT + ": "
                mensaje += respuesta
                fichLog()

                if comprobacion[1] == '100':
                    print("Enviando -> " + LINE3)
                    my_socket.send(bytes(LINE3, 'utf-8') + b'\r\n')
                    aEjecutar = './mp32rtp -i ' + IP + " -p "
                    aEjecutar += comprobacion[9] + ' < ' + FICHERO_AUDIO
                    print("Vamos a ejecutar", aEjecutar)
                    os.system(aEjecutar)

            if METHOD == 'BYE':
                my_socket.connect((str('localhost'), 5555))
                print("Enviando:", str(LINE2))
                my_socket.send(bytes(str(LINE2), 'utf-8') + b'\r\n')
                mensaje = "Sent to " + IP + ":" + PROXY_PORT + ": "
                mensaje += LINE2
                fichLog()
                data = my_socket.recv(1024)  # Tamaño del buffer
                respuesta = data.decode('utf-8')
                print('Recibido -- ', data.decode('utf-8'))
                mensaje = "Received from " + IP + ":" + PROXY_PORT + ": "
                mensaje += respuesta
                fichLog()

    print("Socket terminado.")
